-- MSSQL SCRIPT


CREATE DATABASE inventorydb;
USE inventorydb;
CREATE TABLE products (
    id INT PRIMARY KEY IDENTITY,  
    name VARCHAR(100) NOT NULL,  
    description VARCHAR(200),   
    price DECIMAL(10, 2),  
    quantity INT                 
);
Go;


