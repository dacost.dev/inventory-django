## Django Product CRUD Application

This is a Django application for managing an inventory of products. It allows users to create, read, update, and delete products.

![Swagger Page](./assets/image2.png)

### Features

- **Product List:** View a dynamic list of products with details (name, description, price, quantity).
- **Product By Id:** View one products by its own id.
- **Create Product:** Add new products by entering information in a form.
- **Update Product:** Edit existing product details through a form.
- **Delete Product:** Remove products from the inventory.

### Technologies

- Django
- MSSQL

### Installation

1. **Prerequisites:**

   - Python (latest)
   - MSSQL (latest)

2. **Clone the Repository:**

   ```bash
   git clone https://github.com/dacost.dev/inventory-django.git
   ```

3. **Create a Virtual Environment (Recommended):**

   ```bash
   python -m venv venv
   source venv/bin/activate
   ```

4. **Install Dependencies:**

   ```bash
   pip install -r requirements.txt
   ```

5. **Apply Migrations before the db has been created:**

   ```bash
   python manage.py makemigrations
   python manage.py migrate
   ```

6. **Run the Development Server:**

   ```bash
   python manage.py runserver
   ```

### Usage

1. Visit `http://localhost:8000/` swagger view console, You can now create, read, update, and delete products using the swagger interface.
1. Visit `http://localhost:8000/redocs` automatic documentation.

### License

This project is licensed under the MIT License (see `LICENSE.md` for details).
