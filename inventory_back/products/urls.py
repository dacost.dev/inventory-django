from django.urls import path
from . import views

urlpatterns = [
    path('product/get_all', views.get_products),
    path('product/get_by_id/<int:id>/', views.get_product_by_id),
    path('product/create', views.create_product),
    path('product/update', views.update_product),
    path('product/delete/<int:id>/', views.delete_product),
]