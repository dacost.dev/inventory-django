from django.shortcuts import  get_object_or_404

from inventory_back.products.serializers import ProductSerializer
from .models import Product
from .serializers import ProductSerializer 

from drf_yasg.utils import swagger_auto_schema
from drf_yasg import openapi

from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response


@swagger_auto_schema(
    method='get',
    tags=['Products'], 
    operation_id='Get All Products',
    responses={
        201: openapi.Response('Products retrieved successfully'),
        400: openapi.Response('Bad Request'),
    },
)
@api_view(['GET'])
def get_products(request):
    products = Product.objects.all()
    serialized_products = ProductSerializer(products, many=True)
    return Response(serialized_products.data)


@swagger_auto_schema(
    method='get',
    tags=['Products'], 
    operation_id='Get Product by ID',
    manual_parameters=[
        openapi.Parameter(
            'id',
            openapi.IN_PATH,
            type=openapi.TYPE_INTEGER,
            description='ID of the product to retrieve',
        ),
    ],
    responses={
        200: openapi.Response('Product retrieved successfully', ProductSerializer),
        404: openapi.Response('Product not found'),
    },
)
@api_view(['GET'])
def get_product_by_id(request, id):
    product = get_object_or_404(Product, pk=id)
    serialized_product = ProductSerializer(product)
    return Response(serialized_product.data)


@swagger_auto_schema(
    method='post',
    tags=['Products'], 
    operation_id='Create Product',
    request_body=ProductSerializer,
    responses={
        201: openapi.Response('Product created successfully'),
        400: openapi.Response('Bad Request'),
    },
)
@api_view(['POST']) 
def create_product(request):
    if request.method == 'POST':
        serializer = ProductSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)  


@swagger_auto_schema(
    method='put',
    operation_id='Update Product',
    tags=['Products'], 
    request_body=ProductSerializer,
    responses={
        200: openapi.Response('Product updated successfully', ProductSerializer),
        400: openapi.Response('Bad Request'),
        404: openapi.Response('Product not found'),
    },
)
@api_view(['PUT'])
def update_product(request):
    product_id = request.data.get('id') 
    if product_id is None:
        return Response({'error': 'ID is required'}, status=status.HTTP_400_BAD_REQUEST)
    
    product = get_object_or_404(Product, pk=product_id)
    serializer = ProductSerializer(product, data=request.data)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@swagger_auto_schema(
    method='delete',
    tags=['Products'], 
    operation_id='Delete Product',
    manual_parameters=[
        openapi.Parameter(
            'id',
            openapi.IN_PATH,
            description="ID of the product to delete",
            type=openapi.TYPE_INTEGER,
            required=True,
        ),
    ],
    responses={
        204: openapi.Response('Product deleted successfully'),
        404: openapi.Response('Product not found'),
    },
)
@api_view(['DELETE'])
def delete_product(request, id):
    product = get_object_or_404(Product, pk=id)
    product.delete()
    return Response(status=status.HTTP_204_NO_CONTENT)
