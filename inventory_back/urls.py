from django.contrib import admin
from django.urls import path, include
from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi

schema_view = get_schema_view(
    openapi.Info(
        title="Product CRUD API",
        default_version='v1',
        contact=openapi.Contact(email="dpeerdev@gmail.com"),
        license=openapi.License(name="MIT License"),
    ),
    public=True,
    authentication_classes=(),
)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/v1/', include('inventory_back.products.urls')),
    path('', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    path('redoc/', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),
]
